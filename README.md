# Detecting and Handling Moves
*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---
An example pipeline showing how to post-process a delta file to detect and mark data that has been moved.

This document describes how to run the sample. For concept details see: [Detecting and Handling Moves](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/detecting-and-handling-moves)

The sample code shows how to detect and handle moves. Two different ways of achieving the same result are provided. The first method shown is how to use the Pipelined Comparator and a DXP file to configure the pipeline. The second method shows how to use the Java API and the Document Comparator.


If you have Ant installed, use the build script provided to run the sample. Simply type the following command.

	ant run
	
If you don't have Ant installed, you can run the sample from a command line by issuing commands from the sample directory.

## Pipelined Comparator
The input data files, r1.xml and r2.xml, are located in the pipelined directory.

If you have Ant installed, you may use the build script provided to run the sample by simply typing the following command. This will run the pipeline and produce the output file result.xml in the pipelined directory.

	ant run-dxp
	
If you don't have Ant installed, you can run the sample from a command line by issuing the following command from the sample directory (ensuring that you use the correct directory and class path separators for your operating system).
	
	java -jar ../../command-x.y.z.jar compare moves pipelined/r1.xml pipelined/r2.xml pipelined/result.xml
	
Replace the 'x.y.z' in command-x.y.z.jar with the major.minor.patch version number of your release e.g. command-10.0.0.jar
	
## Document Comparator
The input files for the Document Comparator sample are in the document directory. They are called doc1.xml and doc2.xml and are in DocBook format.

To run just the Document Comparator sample if you have Ant installed, simply type the following command to run the pipeline and produce the output file result.xml in the document directory.

	ant run-dc
	
If you don't have Ant installed, you can run the sample from a command line by issuing the following command from the sample directory (ensuring that you use the correct directory and class path separators for your operating system).

	mkdir bin
	javac -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar -d bin ./src/java/com/deltaxml/samples/HandleMovesSample.java
	java -cp bin:../../deltaxml-x.y.z.jar:../../saxon9pe.jar:../../icu4j.jar:../../resolver.jar:./bin/com/deltaxml/samples/ com.deltaxml.samples.HandleMovesSample document/doc1.xml document/doc2.xml document/result.xml
	
Replace the 'x.y.z' in deltaxml-x.y.z.jar with the major.minor.patch version number of your release e.g. command-10.0.0.jar
	
To clean up the sample directory, run the following Ant command.

	ant clean
