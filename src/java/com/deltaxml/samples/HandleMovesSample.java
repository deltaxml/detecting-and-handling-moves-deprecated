// Copyright (c) 2024 Deltaman group limited. All rights reserved.

package com.deltaxml.samples;

import java.io.File;

import com.deltaxml.cores9api.DocumentComparator;
import com.deltaxml.cores9api.config.ResultReadabilityOptions;

import net.sf.saxon.s9api.Serializer;

public class HandleMovesSample {

  public static void main(String[] args) throws Exception {
    if (args.length < 3) {
      System.out.println("Usage: HandleMovesSample <XML input file 1> <XML input file 2> <result file>");
      throw new Exception("Command line arguments missing");
    }
    String inputFileName1= args[0];
    String inputFileName2= args[1];
    String outputFileName= args[2];

    File input1= new File(inputFileName1);
    File input2= new File(inputFileName2);

    DocumentComparator comparator= new DocumentComparator();

    comparator.setOutputProperty(Serializer.Property.INDENT, "yes");

    ResultReadabilityOptions options= comparator.getResultReadabilityOptions();
    options.setDetectMoves(true);
    options.setMoveAttributeXpath("@*[namespace-uri(.) eq 'http://www.w3.org/XML/1998/namespace' and local-name(.) eq 'id']");

    comparator.compare(input1, input2, new File(outputFileName));
  }

}
