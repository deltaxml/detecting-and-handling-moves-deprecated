<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  version="2.0">
  
  <!-- This (input) filter removes delta attributes so that the extension pipeline comparator can process delta
       file fragments (as an output filter) -->
  
  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@*, node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="@deltaxml:deltaV2"/>

</xsl:stylesheet>